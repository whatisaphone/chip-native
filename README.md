# chip

This is a rust translation of parts of [Lobot], created by Sam Mish (aka chip).
His implementation notes can be found [here][blog].

[Lobot]: https://github.com/samuelpmish/Lobot
[blog]: https://samuelpmish.github.io/notes/RocketLeague/

## Design goals

Where possible, prefer direct transliteration of code rather than cleaning it
up, even if the resulting code is not idiomatic rust. This will ease updates to
future versions of Lobot.

use nalgebra::Isometry3;
use ncollide3d::{
    bounding_volume::{BoundingVolumeInterferencesCollector, HasBoundingVolume, AABB},
    partitioning::BVT,
    query::contact,
    shape::Shape,
};
use std::marker::PhantomData;

pub struct BVH<T>(BVT<i32, AABB<f32>>, PhantomData<T>)
where
    T: Shape<f32> + HasBoundingVolume<f32, AABB<f32>>;

impl<T> BVH<T>
where
    T: Shape<f32> + HasBoundingVolume<f32, AABB<f32>>,
{
    pub fn new(primitives: &[T]) -> BVH<T> {
        let bvt = BVT::new_balanced(
            primitives
                .iter()
                .enumerate()
                .map(|(i, p)| (i as i32, p.bounding_volume(&Isometry3::identity())))
                .collect(),
        );
        BVH(bvt, PhantomData)
    }

    pub fn intersect<S>(&self, query_object: &S) -> Vec<i32>
    where
        S: Shape<f32> + HasBoundingVolume<f32, AABB<f32>>,
    {
        let aabb = query_object.bounding_volume(&Isometry3::identity());
        let mut hits = Vec::new();

        {
            let mut collector = BoundingVolumeInterferencesCollector::new(&aabb, &mut hits);
            self.0.visit(&mut collector);
        }

        // TODO: Narrow phase
        // hits.iter()
        //     .filter(|i| {
        //         let identity = Isometry3::identity();
        //         contact(&identity, self.triangles[i], &identity, query_object,
        //                 0.0).is_some()
        //     }).collect()

        hits
    }
}

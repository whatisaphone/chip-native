// Source:
//   - https://github.com/samuelpmish/Lobot/blob/0d5b058e575493cf0bf34da78270da62e71a80d1/inc/bvh.h
//   - https://github.com/samuelpmish/Lobot/blob/0d5b058e575493cf0bf34da78270da62e71a80d1/src/bvh.cpp
// Status: incomplete

use bit_packing::bits_needed;
use morton;
use nalgebra::Vector2;
use primitives::{Shape, AABB};

fn morton_sort(boxes: &[AABB], global: &AABB) -> Vec<u64> {
    let dim = 3;

    let num_boxes = boxes.len();
    let x_offset = global.min_x;
    let y_offset = global.min_y;
    let z_offset = global.min_z;

    let b = bits_needed(num_boxes as u32);
    let bits_per_dimension = (64 - b) / dim;
    let divisions_per_dimension = 1 << bits_per_dimension;

    let scale = (divisions_per_dimension - 1) as f32;

    let x_scale = scale / (global.max_x - global.min_x);
    let y_scale = scale / (global.max_y - global.min_y);
    let z_scale = scale / (global.max_z - global.min_z);

    let mut code_ids = vec![0; num_boxes];

    for (i, box_) in boxes.iter().enumerate() {
        // get the centroid of the ith bounding box
        let cx = 0.5 * (box_.min_x + box_.max_x);
        let cy = 0.5 * (box_.min_y + box_.max_y);
        let cz = 0.5 * (box_.min_z + box_.max_z);

        let ux = ((cx - x_offset) * x_scale) as u64;
        let uy = ((cy - y_offset) * y_scale) as u64;
        let uz = ((cz - z_offset) * z_scale) as u64;

        let code = morton::encode(ux, uy, uz);

        code_ids.push((code << b) + (i as u64));
    }

    code_ids.sort();

    code_ids
}

pub struct BVH<T> {
    global: AABB,
    mask: u64,
    num_leaves: usize,

    nodes: Vec<BVHNode>,

    ranges: Vec<Vector2<i32>>,
    ready: Vec<i32>,
    parents: Vec<i32>,
    siblings: Vec<i32>,
    code_ids: Vec<u64>,
    primitives: Vec<T>,
}

#[derive(new)]
struct PrefixComparator<'a> {
    base: u64,
    codes: &'a [u64],
    n: i32,
}

impl<'a> PrefixComparator<'a> {
    fn calc(&self, i: i32) -> i32 {
        if i >= 0 && i < self.n {
            (self.base ^ self.codes[i as usize]).leading_zeros() as i32
        } else {
            -1
        }
    }
}

impl<T: Shape> BVH<T> {
    fn build_radix_tree(&mut self) {
        self.parents[self.num_leaves] = self.num_leaves as i32;

        for i in 0..self.num_leaves {
            let i = i as i32;

            let shared_prefix = PrefixComparator::new(
                self.code_ids[i as usize],
                &self.code_ids,
                self.num_leaves as i32,
            );

            // Choose search direction.
            let prefix_prev = shared_prefix.calc(i - 1);
            let prefix_next = shared_prefix.calc(i + 1);
            let prefix_min = i32::min(prefix_prev, prefix_next);

            let d = if prefix_next > prefix_prev { 1 } else { -1 };

            let mut lmax = 32_i32;
            let probe: u32;
            loop {
                lmax <<= 2;
                probe = (i + lmax * d) as u32;
                if !((probe < self.num_leaves as u32) && (shared_prefix.calc(probe as i32) > prefix_min)) {
                    break;
                }
            }

            // Determine length.
            let mut l = 0;
            let mut t = lmax >> 1;
            while t > 0 {
                probe = (i + (l + t) * d) as u32;
                if probe < self.num_leaves as u32 && shared_prefix.calc(probe as i32) > prefix_min {
                    l += t;
                }
                t >>= 1;
            }
            let j = i + l * d;
            let prefix_node = shared_prefix.calc(j);

            // Find split point.
            let s = 0;
            let t = 0;
            loop {
                t = (t + 1) >> 1;
                probe = i + (s + t) * d;
            }
        }
    }

    fn fit_bounding_boxes(&mut self) {
        unimplemented!();
    }
}

fn global_aabb(boxes: &[AABB]) -> AABB {
    let mut boxes = boxes.iter();
    let mut global_box = boxes.next().unwrap().clone();
    for box_ in boxes {
        global_box = AABB::union(&global_box, &box_);
    }
    global_box
}

impl<T: Shape + Clone> BVH<T> {
    pub fn new(_primitives: &[T]) -> BVH<T> {
        let num_leaves = _primitives.len();

        let mut primitives = Vec::with_capacity(num_leaves);

        let mut nodes = Vec::with_capacity(2 * num_leaves - 1);
        let ranges = vec![Vector2::zeros(); 2 * num_leaves - 1];
        let ready = vec![0; 2 * num_leaves - 1];
        let parents = vec![0; 2 * num_leaves - 1];
        let siblings = vec![0; 2 * num_leaves - 1];

        let mask = (1 << bits_needed(num_leaves as u32)) - 1;

        let boxes: Vec<_> = _primitives.iter().map(AABB::new).collect();

        let global = global_aabb(&boxes);

        let code_ids = morton_sort(&boxes, &global);

        for i in 0..num_leaves {
            let id = code_ids[i] & mask;
            primitives.push(_primitives[id as usize].clone());
            nodes.push(BVHNode {
                box_: boxes[id as usize].clone(),
                code: code_ids[i],
            });
        }

        let mut bvh = BVH {
            global,
            mask,
            num_leaves,
            nodes,
            ranges,
            ready,
            parents,
            siblings,
            code_ids,
            primitives,
        };

        bvh.build_radix_tree();
        bvh.fit_bounding_boxes();

        bvh
    }

    pub fn intersect(&self, query_object: &impl Shape) -> Vec<i32> {
        unimplemented!();
    }
}

struct BVHNode {
    box_: AABB,
    code: u64,
}

impl BVHNode {
    fn left(&self) -> i32 {
        (self.code >> 32) as i32
    }

    fn right(&self) -> i32 {
        (self.code & 0xffffffff) as i32
    }
}

use nalgebra::Isometry3;
use ncollide3d::{
    bounding_volume::{HasBoundingVolume, AABB},
    query::distance,
    shape::{Shape, ShapeHandle},
    world::{CollisionGroups, CollisionWorld, GeometricQueryType},
};
use std::marker::PhantomData;
use primitives::Ray;

pub struct BVH<T>(CollisionWorld<f32, i32>, PhantomData<T>)
where
    T: Shape<f32> + HasBoundingVolume<f32, AABB<f32>> + Clone;

impl<T> BVH<T>
where
    T: Shape<f32> + HasBoundingVolume<f32, AABB<f32>> + Clone,
{
    pub fn new(primitives: &[T]) -> BVH<T> {
        let mut walls = CollisionGroups::new();
        walls.set_membership(&[0]);

        let mut world = CollisionWorld::new(1.0);
        for (ti, tri) in primitives.into_iter().enumerate() {
            world.add(
                Isometry3::identity(),
                ShapeHandle::new(tri.clone()),
                walls,
                GeometricQueryType::Proximity(0.0),
                ti as i32,
            );
        }
        world.update();
        BVH(world, PhantomData)
    }

    pub fn raycast_any(&self, query_ray: &Ray) -> Ray {
        let tri = self.0.interferences_with_ray(&From::from(*query_ray), &CollisionGroups::new())
            .next().unwrap();
        unimplemented!();
    }

    pub fn intersect<S>(&self, query_object: &S) -> Vec<i32>
    where
        S: Shape<f32> + HasBoundingVolume<f32, AABB<f32>>,
    {
        let aabb = query_object.bounding_volume(&Isometry3::identity());
        self.0
            .interferences_with_aabb(&aabb, &CollisionGroups::new())
            .filter(|c| {
                distance(
                    &Isometry3::identity(),
                    c.shape().as_ref(),
                    &Isometry3::identity(),
                    query_object,
                ) <= 0.0
            }).map(|c| c.data().clone())
            .collect()
    }
}

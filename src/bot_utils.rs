/*!
chip=/c/Users/John/Documents/Dev/Source/Utilities
for header in ball.h car.h; do
    bindgen \
        "$chip"/cpp/inc/"$header" \ \
        -o src/ffi.rs \
        --no-layout-tests \
        --whitelist-type Ball \
        --whitelist-type Car \
        -- \
        -x c++



    --default-enum-style rust \
    --with-derive-default \
    --raw-line '#![allow(non_camel_case_types, non_snake_case, missing_docs)]' \
    --whitelist-function Interface::IsInitialized \
    --whitelist-function GameFunctions::SetGameState \
    --whitelist-function GameFunctions::StartMatch \
    --whitelist-function GameFunctions::UpdateFieldInfo \
    --whitelist-function GameFunctions::UpdateFieldInfoFlatbuffer \
    --whitelist-function GameFunctions::UpdateLiveDataPacket \
    --whitelist-function GameFunctions::UpdateLiveDataPacketFlatbuffer \
    --whitelist-function GameFunctions::SendQuickChat \
    --whitelist-function GameFunctions::SendChat \
    --whitelist-function GameFunctions::UpdatePlayerInput \
    --whitelist-function GameFunctions::UpdatePlayerInputFlatbuffer \
    --whitelist-function RenderFunctions::RenderGroup \
*/

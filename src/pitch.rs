// Source:
//   - https://github.com/samuelpmish/Lobot/blob/0d5b058e575493cf0bf34da78270da62e71a80d1/inc/pitch.h
//   - https://github.com/samuelpmish/Lobot/blob/0d5b058e575493cf0bf34da78270da62e71a80d1/src/pitch.cpp
// Status: complete

use antidote::Mutex;
use bvh::BVH;
use mesh::MESH_DATA;
use nalgebra::Vector3;
use primitives::{Ray, Shape, Tri};
use std::slice;
use primitives::AABB;

lazy_static! {
    /// Use a singleton so the expensive operation of building the BVH only
    /// needs to happen once.
    pub static ref PITCH: Mutex<Pitch> = Mutex::new(Pitch::new());
}

pub struct Pitch {
    empty: AABB,
    mesh: BVH<Tri>,
    hits: Vec<i32>,
    triangles: &'static [Tri],
}

impl Pitch {
    pub fn new() -> Pitch {
        let triangles = unsafe { slice::from_raw_parts(MESH_DATA.as_ptr() as *const Tri, 8028) };

        Pitch {
            empty: Default::default(),
            mesh: BVH::new(triangles),
            hits: Vec::new(),
            triangles,
        }
    }

    pub fn in_contact_with(&mut self, s: &impl Shape) -> bool {
        self.hits = self.mesh.intersect(s);
        self.hits.len() > 0
    }

    pub fn last_contact_info(&self) -> Ray {
        let mut count = 0.0;
        let mut pos = Vector3::zeros();
        let mut normal = Vector3::zeros();

        for &hit in self.hits.iter() {
            normal += self.triangles[hit as usize].unit_normal();
            pos += self.triangles[hit as usize].center();
            count += 1.0;
        }

        Ray::new(pos / count, normal.normalize())
    }

    pub fn raycast_any(&self, r: &Ray) -> Ray {
        self.mesh.raycast_any(r)
    }
}

use nalgebra::{Matrix3, Vector3};

#[derive(Copy, Clone, new)]
pub struct Ray {
    pub start: Vector3<f32>,
    pub direction: Vector3<f32>,
}

impl Default for Ray {
    fn default() -> Self {
        Ray { start: Vector3::zeros(), direction: Vector3::zeros()}
    }
}

pub struct Obb {
    pub center: Vector3<f32>,
    pub half_width: Vector3<f32>,
    pub orientation: Matrix3<f32>,
}

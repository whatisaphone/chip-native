// Source:
//   - https://github.com/samuelpmish/Lobot/blob/0d5b058e575493cf0bf34da78270da62e71a80d1/inc/primitives.h
//   - https://github.com/samuelpmish/Lobot/blob/0d5b058e575493cf0bf34da78270da62e71a80d1/src/primitives.cpp
// Status: incomplete

use nalgebra::{Isometry3, Vector3,Point3};
use ncollide3d::{
    bounding_volume::{BoundingSphere, HasBoundingVolume, AABB as NAABB},
    query::{PointQuery, RayCast},
    shape::{
        Ball, CompositeShape, Compound, ConvexPolyhedron, Shape as NShape, ShapeHandle, SupportMap,
        Triangle,
    },
};

pub trait Shape: HasBoundingVolume<f32, NAABB<f32>> + NShape<f32> {}

impl Shape for Compound<f32> {}

impl From<::primitives::Ray> for ::ncollide3d::query::Ray<f32> {
    fn from(src: ::primitives::Ray) -> Self {
        Self { origin: Point3::from_coordinates(src.start), dir: src.direction }
    }
}

pub struct Sphere {
    _stub: (),
}

impl Sphere {
    pub fn new(center: Vector3<f32>, radius: f32) -> impl Shape {
        Compound::new(vec![(
            Isometry3::new(center, Vector3::zeros()),
            ShapeHandle::new(Ball::new(radius)),
        )])
    }
}

#[derive(Clone)]
pub struct Tri(Triangle<f32>);

impl Tri {
    pub fn center(&self) -> Vector3<f32> {
        (self.0.a().coords + self.0.b().coords + self.0.c().coords) / 3.0
    }

    pub fn unit_normal(&self) -> Vector3<f32> {
        (self.0.b().coords - self.0.a().coords)
            .cross(&(self.0.c().coords - self.0.a().coords))
            .normalize()
    }
}

impl HasBoundingVolume<f32, NAABB<f32>> for Tri {
    fn bounding_volume(&self, m: &Isometry3<f32>) -> NAABB<f32> {
        self.0.bounding_volume(m)
    }
}

impl NShape<f32> for Tri {
    fn aabb(&self, m: &Isometry3<f32>) -> NAABB<f32> {
        self.0.aabb(m)
    }

    fn bounding_sphere(&self, m: &Isometry3<f32>) -> BoundingSphere<f32> {
        self.0.bounding_sphere(m)
    }

    fn subshape_transform(&self, s: usize) -> Option<Isometry3<f32>> {
        self.0.subshape_transform(s)
    }

    fn as_ray_cast(&self) -> Option<&RayCast<f32>> {
        self.0.as_ray_cast()
    }

    fn as_point_query(&self) -> Option<&PointQuery<f32>> {
        self.0.as_point_query()
    }

    fn as_convex_polyhedron(&self) -> Option<&ConvexPolyhedron<f32>> {
        self.0.as_convex_polyhedron()
    }

    fn as_support_map(&self) -> Option<&SupportMap<f32>> {
        self.0.as_support_map()
    }

    fn as_composite_shape(&self) -> Option<&CompositeShape<f32>> {
        self.0.as_composite_shape()
    }

    fn is_convex_polyhedron(&self) -> bool {
        self.0.is_convex_polyhedron()
    }

    fn is_support_map(&self) -> bool {
        self.0.is_support_map()
    }

    fn is_composite_shape(&self) -> bool {
        self.0.is_composite_shape()
    }
}

pub struct AABB(NAABB<f32>);

impl Default for AABB {
    fn default() -> Self {
        AABB(NAABB::new(Point3::origin(), Point3::origin()))
    }
}

// Source:
//   - https://github.com/samuelpmish/Lobot/blob/0d5b058e575493cf0bf34da78270da62e71a80d1/inc/primitives.h
//   - https://github.com/samuelpmish/Lobot/blob/0d5b058e575493cf0bf34da78270da62e71a80d1/src/primitives.cpp
// Status: incomplete

use linalg::left_dot;
use nalgebra::Vector3;
use primitives::common::Obb;
use std::ops::RangeInclusive;

pub trait Shape {
    fn as_sphere(&self) -> Option<&Sphere> {
        None
    }

    fn as_tri(&self) -> Option<&Tri> {
        None
    }

    fn as_obb(&self) -> Option<&Obb> {
        None
    }
}

#[derive(new)]
pub struct Sphere {
    pub center: Vector3<f32>,
    pub radius: f32,
}

impl Shape for Sphere {
    fn as_sphere(&self) -> Option<&Sphere> {
        Some(self)
    }
}

#[derive(Clone)]
pub struct Tri {
    p: [Vector3<f32>; 3],
}

impl Shape for Tri {
    fn as_tri(&self) -> Option<&Tri> {
        Some(self)
    }
}

impl Tri {
    pub fn center(&self) -> Vector3<f32> {
        (self.p[0] + self.p[1] + self.p[2]) / 3.0
    }

    pub fn unit_normal(&self) -> Vector3<f32> {
        (self.p[1] - self.p[0])
            .cross(&(self.p[2] - self.p[0]))
            .normalize()
    }
}

#[derive(Clone, Default)]
pub struct AABB {
    pub min_x: f32,
    pub min_y: f32,
    pub min_z: f32,

    pub max_x: f32,
    pub max_y: f32,
    pub max_z: f32,
}

impl AABB {
    pub fn new(s: &impl Shape) -> Self {
        if let Some(t) = s.as_tri() {
            Self::from_tri(t)
        } else if let Some(o) = s.as_obb() {
            Self::from_obb(o)
        } else if let Some(s) = s.as_sphere() {
            Self::from_sphere(s)
        } else {
            unimplemented!()
        }
    }

    pub fn from_tri(t: &Tri) -> Self {
        Self {
            min_x: (t.p[0].x).min(t.p[1].x).min(t.p[2].x),
            min_y: (t.p[0].y).min(t.p[1].y).min(t.p[2].y),
            min_z: (t.p[0].z).min(t.p[1].z).min(t.p[2].z),

            max_x: (t.p[0].x).max(t.p[1].x).max(t.p[2].x),
            max_y: (t.p[0].y).max(t.p[1].y).max(t.p[2].y),
            max_z: (t.p[0].z).max(t.p[1].z).max(t.p[2].z),
        }
    }

    pub fn from_obb(o: &Obb) -> Self {
        let x_range = project_along_axis_obb(o, Vector3::new(1.0, 0.0, 0.0));
        let y_range = project_along_axis_obb(o, Vector3::new(0.0, 1.0, 0.0));
        let z_range = project_along_axis_obb(o, Vector3::new(0.0, 0.0, 1.0));

        Self {
            min_x: *x_range.start(),
            min_y: *y_range.start(),
            min_z: *z_range.start(),

            max_x: *x_range.end(),
            max_y: *y_range.end(),
            max_z: *z_range.end(),
        }
    }

    pub fn from_sphere(s: &Sphere) -> Self {
        Self {
            min_x: s.center.x - s.radius,
            min_y: s.center.y - s.radius,
            min_z: s.center.z - s.radius,

            max_x: s.center.x + s.radius,
            max_y: s.center.y + s.radius,
            max_z: s.center.z + s.radius,
        }
    }

    pub fn union(a: &AABB, b: &AABB) -> Self {
        AABB {
            min_x: f32::min(a.min_x, b.min_x),
            min_y: f32::min(a.min_y, b.min_y),
            min_z: f32::min(a.min_z, b.min_z),
            max_x: f32::max(a.max_x, b.max_x),
            max_y: f32::max(a.max_y, b.max_y),
            max_z: f32::max(a.max_z, b.max_z),
        }
    }
}

fn project_along_axis_aabb(a: &AABB, axis: Vector3<f32>) -> RangeInclusive<f32> {
    let abs_axis = axis.abs();
    let center = 0.5 * Vector3::new(a.max_x + a.min_x, a.max_y + a.min_y, a.max_z + a.min_z);
    let diagonal = Vector3::new(a.max_x - a.min_x, a.max_y - a.min_y, a.max_z - a.min_z);

    let i_center = axis.dot(&center);
    let i_radius = 0.5 * abs_axis.dot(&diagonal);

    (i_center - i_radius)..=(i_center + i_radius)
}

fn project_along_axis_obb(a: &Obb, axis: Vector3<f32>) -> RangeInclusive<f32> {
    let raxis = left_dot(axis, a.orientation);
    let abs_raxis = raxis.abs();

    let i_center = axis.dot(&a.center);
    let i_radius = abs_raxis.dot(&a.half_width);

    (i_center - i_radius)..=(i_center + i_radius)
}

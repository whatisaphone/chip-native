// Source:
//   - https://github.com/samuelpmish/Lobot/blob/0d5b058e575493cf0bf34da78270da62e71a80d1/inc/Car.h
//   - https://github.com/samuelpmish/Lobot/blob/0d5b058e575493cf0bf34da78270da62e71a80d1/src/Car.cpp
// Status: complete
//
// Check if nalgebra already does what you need before adding anything here.

use nalgebra::{Matrix3, Vector3};

pub fn left_dot(v: Vector3<f32>, a: Matrix3<f32>) -> Vector3<f32> {
    let mut va = Vector3::zeros();
    for i in 0..3 {
        for j in 0..3 {
            va[i] += v[j] * a[(j, i)]
        }
    }
    va
}

pub fn sgn(x: f32) -> f32 {
    ((0.0 < x) as i32 as f32) - ((x < 0.0) as i32 as f32)
}

pub fn axis_rotation(omega: Vector3<f32>) -> Matrix3<f32> {
    let theta = omega.norm();

    if theta.abs() < 0.000001 {
        return Matrix3::identity();
    }

    let axis = omega.normalize();

    let k = Matrix3::new(
        0.0, -axis[2], axis[1], axis[2], 0.0, -axis[0], -axis[1], axis[0], 0.0,
    );

    Matrix3::identity() + theta.sin() * k + (1.0 - theta.cos()) * (k * k)
}

pub fn lerp(a: f32, b: f32, t: f32) -> f32 {
    a * (1.0 - t) + b * t
}

// Source:
//   - https://github.com/samuelpmish/Lobot/blob/0d5b058e575493cf0bf34da78270da62e71a80d1/inc/vec.h
// Status: complete
//
// Check if nalgebra already does what you need before adding anything here.

use nalgebra::Vector3;

pub fn elementwise_mul(lhs: Vector3<f32>, rhs: Vector3<f32>) -> Vector3<f32> {
    Vector3::new(lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z)
}

// Source:
//   - https://github.com/samuelpmish/Lobot/blob/0d5b058e575493cf0bf34da78270da62e71a80d1/inc/Car.h
//   - https://github.com/samuelpmish/Lobot/blob/0d5b058e575493cf0bf34da78270da62e71a80d1/src/Car.cpp
// Status: complete

use linalg::{axis_rotation, left_dot, lerp, sgn};
use nalgebra::{clamp, Matrix3, Vector3};
use pitch::PITCH;
use primitives::{Obb, Sphere};
use rlbot::PlayerInput;
use vec::elementwise_mul;

const DELTA_T: f32 = 0.01666;

const G: f32 = -650.0;
const W_MAX: f32 = 5.5;
const BOOST_ACCEL: f32 = 1000.0;
const DODGE_LIMIT: f32 = 0.5; // ?

pub struct Car {
    pub x: Vector3<f32>,
    pub v: Vector3<f32>,
    pub w: Vector3<f32>,
    pub o: Matrix3<f32>,

    pub supersonic: bool,
    pub jumped: bool,
    pub double_jumped: bool,
    pub on_ground: bool,

    pub boost: i32,

    pub can_dodge: bool,
    pub dodge_timer: f32,

    pub sphere_collider_radius: f32,
    pub obb_collider: Obb,
    pub pivot_offset: Vector3<f32>,

    pub time: f32,
    pub eta: f32,

    pub team: i32,
    pub id: i32,

    pub target: Vector3<f32>,
    pub target_normal: Vector3<f32>,
    pub target_speed: f32,

    pub last: PlayerInput,
}

fn max_curvature(speed: f32) -> f32 {
    const N: usize = 6;
    let values: [[f32; 2]; N] = [
        [0.0, 0.00690],
        [500.0, 0.00398],
        [1000.0, 0.00235],
        [1500.0, 0.00138],
        [1750.0, 0.00110],
        [2500.0, 0.00080],
    ];

    let input = clamp(speed, 0.0, 2500.0);

    for i in 0..(N - 1) {
        if values[i][0] <= input && input < values[i + 1][0] {
            let u = (input - values[i][0]) / (values[i + 1][0] - values[i][0]);
            return lerp(values[i][1], values[i + 1][1], u);
        }
    }

    return -1.0;
}

fn max_speed(curvature: f32) -> f32 {
    const N: usize = 6;
    let values: [[f32; 2]; N] = [
        [0.0, 0.00690],
        [500.0, 0.00398],
        [1000.0, 0.00235],
        [1500.0, 0.00138],
        [1750.0, 0.00110],
        [2500.0, 0.00080],
    ];

    let input = clamp(curvature, 0.00080, 0.00690);

    for i in 0..(N - 1) {
        if values[i][1] <= input && input < values[i + 1][1] {
            let u = (input - values[i][1]) / (values[i + 1][1] - values[i][1]);
            return clamp(lerp(values[i][0], values[i + 1][0], u), 0.0, 2300.0);
        }
    }

    return -1.0;
}

impl Car {
    pub fn forward(&self) -> Vector3<f32> {
        Vector3::new(self.o[(0, 0)], self.o[(1, 0)], self.o[(2, 0)])
    }

    pub fn left(&self) -> Vector3<f32> {
        Vector3::new(self.o[(0, 1)], self.o[(1, 1)], self.o[(2, 1)])
    }

    pub fn up(&self) -> Vector3<f32> {
        Vector3::new(self.o[(0, 2)], self.o[(1, 2)], self.o[(2, 2)])
    }

    fn jump(&self, _inp: &PlayerInput, _dt: f32) {
        // TODO
    }

    fn air_dodge(&self, _inp: &PlayerInput, _dt: f32) {
        // TODO
    }

    fn aerial_control(&mut self, inp: &PlayerInput, dt: f32) {
        const M: f32 = 180.0; // mass
        const J: f32 = 10.5; // moment of inertia
        const V_MAX: f32 = 2300.0;
        const W_MAX: f32 = 5.5;
        const BOOST_FORCE: f32 = 178500.0;
        const THROTTLE_FORCE: f32 = 12000.0;
        let g = Vector3::new(0.0, 0.0, -651.47);

        let rpy = Vector3::new(inp.Roll, inp.Pitch, inp.Yaw);

        // air control torque coefficients
        let t = Vector3::new(-400.0, -130.0, 95.0);

        // air damping torque coefficients
        let h = Vector3::new(
            -50.0,
            -30.0 * (1.0 - inp.Pitch.abs()),
            -20.0 * (1.0 - inp.Yaw.abs()),
        );

        let thrust;

        if inp.Boost && self.boost > 0 {
            thrust = BOOST_FORCE + THROTTLE_FORCE;
            self.boost -= 1;
        } else {
            thrust = inp.Throttle * THROTTLE_FORCE;
        }

        self.v += (g + (thrust / M) * self.forward()) * dt;
        self.x += self.v * dt;

        let w_local = left_dot(self.w, self.o);

        let old_w = self.w;
        self.w += self.o * (elementwise_mul(t, rpy) + elementwise_mul(h, w_local)) * (dt / J);

        let r = axis_rotation(0.5 * (self.w + old_w) * dt);

        self.o = axis_rotation(0.5 * (self.w + old_w) * dt) * self.o;

        // if the velocities exceed their maximum values, scale them back
        self.v /= f32::max(1.0, self.v.norm() / V_MAX);
        self.w /= f32::max(1.0, self.w.norm() / W_MAX);
    }

    fn drive_force_forward(&self, inp: &PlayerInput) -> f32 {
        const DRIVING_SPEED: f32 = 1450.0;
        const BRAKING_FORCE: f32 = -3500.0;
        const COASTING_FORCE: f32 = -525.0;
        const THROTTLE_THRESHOLD: f32 = 0.05;
        const THROTTLE_FORCE: f32 = 1550.0;
        const MAX_SPEED: f32 = 2275.0;
        const MIN_SPEED: f32 = 10.0;
        const BOOST_FORCE: f32 = 1500.0;
        const STEERING_TORQUE: f32 = 25.75;
        const BRAKING_THRESHOLD: f32 = -0.001;
        const SUPERSONIC_TURN_DRAG: f32 = -98.25; // TODO

        let v_f = self.v.dot(&self.forward());
        let v_l = self.v.dot(&self.left());
        let w_u = self.w.dot(&self.up());

        let dir = sgn(v_f);
        let speed = v_f.abs();

        let turn_damping = (-0.07186693033945346 * inp.Steer.abs()
            + -0.05545323728191764 * w_u.abs()
            + 0.00062552963716722 * v_l.abs())
            * v_f;

        // boosting
        if inp.Boost {
            if v_f < 0.0 {
                return -BRAKING_FORCE;
            } else {
                if v_f < DRIVING_SPEED {
                    return MAX_SPEED - v_f;
                } else {
                    if v_f < MAX_SPEED {
                        return BOOST_ACCEL + turn_damping;
                    } else {
                        // ?
                        return SUPERSONIC_TURN_DRAG * w_u.abs();
                        // ?
                    }
                }
            }
        // not boosting
        } else {
            // braking
            if inp.Throttle * sgn(v_f) <= BRAKING_THRESHOLD && v_f.abs() > MIN_SPEED {
                return BRAKING_FORCE * sgn(v_f);
            // not braking
            } else {
                // coasting
                if inp.Throttle.abs() < THROTTLE_THRESHOLD && v_f.abs() > MIN_SPEED {
                    return COASTING_FORCE * sgn(v_f) + turn_damping;
                // accelerating
                } else {
                    if v_f.abs() > DRIVING_SPEED {
                        return turn_damping;
                    } else {
                        return inp.Throttle * (THROTTLE_FORCE - v_f.abs()) + turn_damping;
                    }
                }
            }
        }
    }

    fn drive_force_left(&self, inp: &PlayerInput) -> f32 {
        let v_f = self.v.dot(&self.forward());
        let v_l = self.v.dot(&self.left());
        let w_u = self.w.dot(&self.up());

        return (1380.4531378 * inp.Steer + 7.8281188 * inp.Throttle - 15.0064029 * v_l
            + 668.1208332 * w_u)
            * (1.0 - f32::exp(-0.001161 * v_f.abs()));
    }

    fn drive_torque_up(&self, inp: &PlayerInput) -> f32 {
        let v_f = self.v.dot(&self.forward());
        let w_u = self.w.dot(&self.up());

        15.0 * (inp.Steer * max_curvature(v_f.abs()) * v_f - w_u)
    }

    fn driving(&mut self, inp: &PlayerInput, dt: f32) {
        // in-plane forces
        let force = self.drive_force_forward(inp) * self.forward()
            + self.drive_force_left(inp) * self.left();

        // out-of-plane torque
        let torque = self.drive_torque_up(inp) * self.up();

        self.v += force * dt;
        self.x += self.v * dt;

        self.w += torque * dt;
        self.o = axis_rotation(self.w * dt) * self.o;
    }

    fn driving_handbrake(&mut self, _inp: &PlayerInput, _dt: f32) {
        // TODO
    }

    pub fn step(&mut self, inp: &PlayerInput, dt: f32) {
        // driving
        if self.on_ground {
            if inp.Jump {
                self.jump(inp, dt);
            } else {
                if !inp.Handbrake {
                    self.driving(inp, dt);
                } else {
                    self.driving_handbrake(inp, dt);
                }
            }
        // in the air
        } else {
            if inp.Jump && self.dodge_timer < DODGE_LIMIT && self.can_dodge {
                self.air_dodge(inp, dt);
            } else {
                self.aerial_control(inp, dt);
            }
        }
    }

    pub fn pitch_surface_normal(&self) -> Vector3<f32> {
        let mut env = PITCH.lock();
        if env.in_contact_with(&Sphere::new(self.x, self.sphere_collider_radius)) {
            env.last_contact_info().direction
        } else {
            Vector3::zeros()
        }
    }

    pub fn bounding_box(&self) -> Obb {
        Obb {
            orientation: self.o,
            half_width: self.obb_collider.half_width,
            center: self.o * self.pivot_offset + self.x,
        }
    }

    pub fn extrapolate(&mut self, dt: f32) {
        let last = self.last;
        self.step(&last, dt);
    }

    pub fn new() -> Self {
        Self {
            x: Vector3::zeros(),
            v: Vector3::zeros(),
            w: Vector3::zeros(),
            o: Matrix3::identity(),

            supersonic: false,
            jumped: false,
            double_jumped: false,
            on_ground: false,

            boost: 0,

            can_dodge: false,
            dodge_timer: 0.0,

            obb_collider: Obb {
                center: Vector3::zeros(),
                half_width: Vector3::new(59.00368881, 42.09970474, 18.07953644),
                orientation: Matrix3::identity(),
            },
            pivot_offset: Vector3::new(13.97565993, 0.0, 20.75498772),

            // just approximate geometry here
            sphere_collider_radius: 40.0,

            last: Default::default(),

            eta: 0.0,
            time: 0.0,

            team: 0,
            id: 0,

            target: Vector3::zeros(),
            target_normal: Vector3::zeros(),

            target_speed: 0.0,
        }
    }
}

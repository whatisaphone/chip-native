// Source:
//   - https://github.com/samuelpmish/Utilities/blob/015710b5be53488f47e6763b7621a11315debe87/cpp/inc/Car.h
// Status: complete

use nalgebra::Vector3;
use pitch::PITCH;
use primitives::{Ray, Shape, Sphere};

pub struct Ball {
    // `Ball::p` has been replaced with `pitch::PITCH`.
    pub x: Vector3<f32>,
    pub v: Vector3<f32>,
    pub w: Vector3<f32>,
    pub t: f32,
    pub last_bounce: Ray,
    pub radius: f32,
}

impl Ball {
    pub fn new() -> Self {
        let radius = 91.25;
        Self {
            radius,

            x: Vector3::new(0.0, 0.0, 1.1 * radius),
            v: Vector3::new(0.0, 0.0, 0.0),
            w: Vector3::new(0.0, 0.0, 0.0),

            last_bounce: Default::default(),
            t: 0.0,
        }
    }

    pub fn collider(&self) -> impl Shape {
        Sphere::new(self.x, self.radius)
    }

    pub fn wall_nearby(&self, r: f32) -> Ray {
        if -3750.0 < self.x[0] && self.x[0] < 3750.0 && -4500.0 < self.x[1] && self.x[1] < 4500.0 && self.x[2] < r {
            return Ray::new(
                Vector3::new(self.x[0], self.x[1], 0.0),
                Vector3::new(0.0, 0.0, 1.0),
            );
        } else {
            let mut p = PITCH.lock();
            if p.in_contact_with(&Sphere::new(self.x, r)) {
                return p.last_contact_info();
            } else {
                return Ray::default();
            }
        }
    }

    pub fn step(&mut self, dt: f32) {
        let r = self.radius; // ball radius
        const G: f32 = -650.0; // gravitational acceleration
        const A: f32 = 0.0003; // inverse moment of inertia
        const Y: f32 = 2.0; // maximum frictional contribution
        const MU: f32 = 0.280; // Coulomb friction coefficient
        const C_R: f32 = 0.6; // coefficient of restitution
        const DRAG: f32 = -0.0305; // velocity-proportional drag coefficient
        const W_MAX: f32 = 6.0; // maximum angular velocity

        if self.v.norm() > 0.0001 {
            let a = DRAG * self.v + Vector3::new(0.0, 0.0, G);
            let v_pred = self.v + a * dt;
            let x_pred = self.x + v_pred * dt;
            let w_pred = self.w;

            let mut p = PITCH.lock();
            if p.in_contact_with(&Sphere::new(x_pred, self.radius)) {
                self.last_bounce = p.last_contact_info();

                let n = self.last_bounce.direction;

                let v_perp = v_pred.dot(&n) * n;
                let v_para = v_pred - v_perp;
                let v_spin = r * n.cross(&w_pred);
                let s = v_para + v_spin;

                let ratio = v_perp.norm() / s.norm();

                let delta_v_perp = -(1.0 + C_R) * v_perp;
                let delta_v_para = -f32::min(1.0, Y * ratio) * MU * s;

                self.w = w_pred + A * r * delta_v_para.cross(&n);
                self.v = v_pred + delta_v_perp + delta_v_para;

                // TODO(SAM)
                self.x = self.x + 0.5 * (self.v + v_pred) * dt;
            // TODO(SAM)
            } else {
                self.w = w_pred;
                self.v = v_pred;
                self.x = x_pred;
            }
        }

        self.w *= f32::min(1.0, W_MAX / self.w.norm());

        self.t += dt;
    }
}

#[cfg(test)]
mod tests {
    use ball::Ball;

    #[test]
    fn ball() {
        let mut ball = Ball::new();
        ball.step(1.0 / 60.0);
    }
}
